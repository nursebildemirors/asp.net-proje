﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(nursebil.Startup))]
namespace nursebil
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
